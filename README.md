# OpenML dataset: Chaos_detection_in_Duffing_system

https://www.openml.org/d/42202

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is an artificial simulation of the Duffing system with random changes from the chaotic to the non-chaotic regime at different noise levels.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42202) of an [OpenML dataset](https://www.openml.org/d/42202). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42202/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42202/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42202/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

